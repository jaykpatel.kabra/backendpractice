import { Container } from "inversify"
import { DepartmentRepository } from "./Department.repository"
import './ApiController'
export const container=new Container()

container.bind<DepartmentRepository>("DepartmentRepository").to(DepartmentRepository)