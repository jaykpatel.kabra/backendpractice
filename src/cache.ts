import redis from "redis"
import bluebird from "bluebird"
bluebird.promisifyAll(redis)

export interface IPromisifiedRedis extends redis.RedisClient {
  [x: string]: any
}

const redisPort = process.env.REDIS_PORT
const redisURL = process.env.REDIS_URL
const redisPwd = process.env.REDIS_PASSWORD

export const RedisClient = redis.createClient(Number(redisPort), redisURL, {
  password: redisPwd,
}) as IPromisifiedRedis

export default RedisClient
