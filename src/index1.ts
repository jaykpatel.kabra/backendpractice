const sioRedis = require("socket.io-redis")
import redis from "redis"
import { app } from "./app"
//import { io } from "./service/rest/helpers/sockets/index"
import { logger } from "./logger"
const port = parseInt(process.env.PORT || "3000")
export const httpServer = app.listen(port, (err: any) => {
  if (err) {
    return logger.error(`Unable to create server: ${err.message}`)
  }
  logger.info(`Server is running on port: ${port}`)
})

const redisPort = process.env.REDIS_PORT
const redisURL = process.env.REDIS_URL
const redisPwd = process.env.REDIS_PASSWORD

// Here we need to fresh redis connection for pubClient and subClient
const redisAdapter = sioRedis({
  pubClient: redis.createClient(Number(redisPort), redisURL, {
    password: redisPwd,
  }),
  subClient: redis.createClient(Number(redisPort), redisURL, {
    password: redisPwd,
  }),
  prefix: "taptap",
})
//io.adapter(redisAdapter)

//io.attach(httpServer)

redisAdapter.pubClient.on("error", function (err: Error) {
  logger.error(`SocketIORedis-pubClient: ${err.message}`)
})
redisAdapter.subClient.on("error", function (err: Error) {
  logger.error(`SocketIORedis-subClient: ${err.message}`)
})
